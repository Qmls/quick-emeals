var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sessions = require('express-session');
var index = require('./routes/index');
var users = require('./routes/users');
var reservations=require('./routes/reservations');
var items=require('./routes/items');
var restaurants=require('./routes/restaurants');
//var mailer=require('express-mailer');
var paypal = require('paypal-rest-sdk');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   res.header('Access-Control-Allow-Headers', 'Content-Type');

   next();
}
//mailer.extend(app.{
//from: 'quick_emeals@gmail.com',
//host: 'smtp.gmail.com',
//secureConnection: true,
//port: 465,
//transportMethod: 'smtp',
//auth:{
  //user: 'quick-emeals@gmail.com',
  //pass:'QuickEmeals33'
//}
//});
paypal.configure({
  'mode': 'live', //sandbox or live
  'client_id': 'EBWKjlELKMYqRNQ6sYvFo64FtaRLRR5BdHEESmha49TM',
  'client_secret': 'EO422dn3gQLgDbuwqTjzrFgFtaRLRR5BdHEESmha49TM'
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(sessions({
 cookieName: 'session',
 secret: 'zemoIOwqc7nTYxHiSKRb9q1czBVyJC7Kl0kE5WvXZi3QoddhuufkOUnFctYTH2XP3aGksBiDEtHrmHT7kr7Zurpf4EIqpn1pVTrpmKhz3WGzNZOJhovzzgw9w4MVeHeHvAIG8ChtQEiNQd0jCKRsqIqWDtfdjCllufgHJ4jbG4WvzzbMMwe9mPiwyvx0vB6fDpsLmuWpNUFWTxooQ61okgMqlNkj0EzkkQbRmAQeEOJuDXygeLm',
 duration: 24 * 60 * 60 * 1000, // ms 24 horas
 activeDuration: 1000 * 60 * 50 //minutos extra
}));

app.use(allowCrossDomain);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/app',express.static("../FrontEnd"));
app.use('/', index);
app.use('/items',items);
app.use('/reservations',reservations);
app.use('/users', users);
app.use('/restaurants', restaurants);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
 // res.render('error');
});

module.exports = app;
