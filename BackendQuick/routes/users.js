var express = require('express');
var router = express.Router();
var sha1=require('sha1');
var userTable=require("../models/users");


router.get('/', function(req, res, next) {
userTable.findAll().then(function(users){
res.json(users);
});
});

router.post('/create', function(req, res, next) {
	var NewUser={ 
//"name"           :req.body.name,
"email"          :req.body.email,          
"password"  	 :sha1(req.body.password),
	};
console.log("Creando usuario...");
  console.log(NewUser);

	userTable.build(NewUser).save().then(function(){
var createJson = {"message":"Se ha creado el usuario con exito!", "created":true };
res.json(createJson);
}).catch(function (err){
	console.log(err);
});
});

router.post('/login', function(req, res, next) {
  var responselogin = {};
  console.log(req.body);

  var query = {"email":req.body.email, "password":sha1(req.body.password) };

  userTable.findOne( { where: query } ).then(function(user)
	{
		if(user){
			responselogin = {"message":"Login exitoso", "login":true };  				 
			req.session.user=user;
		}else{
			responselogin = {"message":"Login fallido", "login":false };  	  	
		}
  		res.json(responselogin);
	});


});
router.get('/whoisdis',function(req,res,next){
if(req.session.user){
res.json({"message":"you is here","username":req.session.user});
}
else{
res.json({"message":"you is not real"});
};
});

router.delete('/User/:id', function(req, res, next) {
var id=req.params.id;

	userTable.destroy({
		where:{
		id:id
	    }
	});  
});
//router.get('/secretplaces',)

module.exports = router;
