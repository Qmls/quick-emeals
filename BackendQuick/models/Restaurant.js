var mysql_host ="localhost";
var Sequelize = require('sequelize');
var itemsTable=require("../models/items");

var sequelize = new Sequelize('QuickDB','root','DoZa27MNG192671', {                 
	host: mysql_host,
	dialect:'mysql'
});


var restaurant = sequelize.define('restaurants',{

"id"                        :{ type: Sequelize.INTEGER, primaryKey:true,autoIncrement:true},
"name"                      :{  type: Sequelize.STRING},
"email"                     :{  type: Sequelize.STRING},
"description"		        :{  type: Sequelize.STRING},
"Phone Number"              :{  type: Sequelize.STRING}, 
"Picture"		            :{  type: Sequelize.BLOB},
"Ubication"		            :{  type: Sequelize.STRING},
"facebookID"				:{	type:Sequelize.STRING},
"facebook_code"				:{	type: Sequelize.TEXT},

}
);
restaurant.belongsToMany(itemsTable, { as: 'Tasks', through: 'worker_tasks', foreignKey: 'IdRes'});
restaurant.sync();
module.exports = restaurant;
;